---
layout: post
title: 'Arbres'
---
Petit guide pour formater les photos : [how to style images with markdown](https://www.xaprb.com/blog/how-to-style-images-with-markdown/)



![Flanc dans la brume](../assets/img/projects/proj-01/img_001.jpeg "Ca c'est du flanc")

<img src="../assets/img/projects/proj-01/img_002.jpeg" alt="Arbre"
	title="Drôle d'arbre" width="100%" height="100%"/>

![](../assets/img/projects/proj-01/img_003.jpeg)

![](../assets/img/projects/proj-01/img_004.jpeg)
