# Antisèche pour mettre à jour le site

## Rajouter un projet

- ajouter les photos dans le dossier `assets/img/projects/proj-n%2`
  - l'image de garde pour le projet doit être nommée `thumbnail.jpg`
  - plusieurs façons d'appeler les images (voir projets 1 et 2)
- créer un fichier `.md` sous la forme `proj-n%2`
- mettre à jour la section *projects* dans le fichier `_data/settings.yml` 

